package com.raymondma_public.trip_android.map;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends BaseActivity {

    private static final String TAG = "MapActivity";
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private double lat;
            private double lng;

//    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//
//            Log.d(TAG, response.toString());
//            // Do a null check to confirm that we have not already instantiated the map.
//            if (mMap == null) {
//                // Try to obtain the map from the SupportMapFragment.
//                mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//                        .getMap();
//                // Check if we were successful in obtaining the map.
//                if (mMap != null) {
//
//                }
//            }
//
//            double latitude = 0;
//            double longitude = 0;
//
//            String toast = "";
//
//            try {
//
//                JSONArray routes = response.getJSONArray("routes");
//                for (int i = 0; i < routes.length(); i++) {
//                    PolylineOptions list = new PolylineOptions().geodesic(true);
//                    JSONArray legs = ((JSONObject) routes.get(i)).getJSONArray("legs");
//                    for (int j = 0; j < legs.length(); j++) {
//
//                        JSONObject start_location = ((JSONObject) legs.get(j)).getJSONObject("start_location");//"location"
//                        double start_lat = start_location.getDouble("lat");
//                        double start_lng = start_location.getDouble("lng");
//                        list.add(new LatLng(start_lat, start_lng));
//                        JSONObject end_location = ((JSONObject) legs.get(j)).getJSONObject("end_location");//"location"
//                        double end_lat = start_location.getDouble("lat");
//                        double end_lng = start_location.getDouble("lng");
//                        list.add(new LatLng(end_lat, end_lng));
//
//                        mMap.addMarker(new MarkerOptions().position(new LatLng(start_lat, start_lng)).title("Marker"));
////                    toast += start_lat + " " + start_lng + " "+end_lat+" "+end_lng+"\n";
//
//                        mMap.addPolyline(list);
//                    }
//                }
//
//                Toast.makeText(TripApplication.getAppContext(), routes.toString(), Toast.LENGTH_SHORT).show();
//
//
//                CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(latitude, longitude), 10);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
//        setUpMapIfNeeded();

        Intent intent=getIntent();
        lat=intent.getDoubleExtra("lat",0);
        lng=intent.getDoubleExtra("long",0);
        if(lat==0 && lng==0){

        }else{
            setUpMapIfNeeded();
        }
//        request();
    }

//    private void request() {
//
//        String requestURL = "https://maps.googleapis.com/maps/api/directions/json?origin=-35.28032,149.12907&destination=-35.28032,149.12907=optimize:true|Barossa+Valley,SA|Clare,SA|22.392321,114.109980|Connawarra,SA|McLaren+Vale,SA&key=AIzaSyB5wd0X7n1Y-luZLQ0JQlgh5fyhANDrbCw";
//
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, requestURL, responseListener, new ErrorToastListener());
////        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
//        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
//                setUpMap();
            }
        }
        Marker marker=mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Here"));
       marker.showInfoWindow();

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(lat, lng),15));

//        // Polylines are useful for marking paths and routes on the map.
//        mMap.addPolyline(new PolylineOptions().geodesic(true)
//                .add(new LatLng(-33.866, 151.195))  // Sydney
//                .add(new LatLng(-18.142, 178.431))  // Fiji
//                .add(new LatLng(21.291, -157.821))  // Hawaii
//                .add(new LatLng(37.423, -122.091))  // Mountain View
//        );
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */

//    private void setUpMap() {
//        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
//    }
}
