package com.raymondma_public.trip_android.newEvent;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.utils.DatePickerFragment;

public class NewEventActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
