package com.raymondma_public.trip_android.volley;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.raymondma_public.trip_android.TripApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by RaymondMa on 1/11/2015.
 */
public class ErrorToastListener implements Response.ErrorListener {

    @Override
    public void onErrorResponse(VolleyError error) {
        try {
            if(error!=null && error.networkResponse!=null) {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject jsonObject = new JSONObject(responseBody);
                String message = jsonObject.getString("message");
                Toast.makeText(TripApplication.getAppContext(), message, Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        error.printStackTrace();
        Toast.makeText(TripApplication.getAppContext(), "Error" + error, Toast.LENGTH_SHORT).show();
        Log.e("News", "Load data error");
    }

}