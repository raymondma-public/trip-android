//package com.raymondma_public.trip_android.volley;
//
//import android.app.Application;
//import android.content.Context;
//
///**
// * Created by HEI on 23/6/2015.
// */
//public class MyApplication extends Application {
//    private static MyApplication mInstance;
//    private static Context mAppContext;
//
//    public static MyApplication getInstance() {
//        return mInstance;
//    }
//
//    public static Context getAppContext() {
//        return mAppContext;
//    }
//
//    public void setAppContext(Context mAppContext) {
//        this.mAppContext = mAppContext;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        mInstance = this;
//
//        this.setAppContext(getApplicationContext());
//    }
//}