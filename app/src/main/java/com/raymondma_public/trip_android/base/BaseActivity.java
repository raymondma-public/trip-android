package com.raymondma_public.trip_android.base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.checklist.CheckListActivity;
import com.raymondma_public.trip_android.diary.DiarysActivity;
import com.raymondma_public.trip_android.login.LoginActivity;

/**
 * Created by HEI on 14/10/2015.
 */
public class BaseActivity extends AppCompatActivity {

    private ImageView logoutBtn;
    private ImageView loginBtn;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (loginBtn != null && logoutBtn != null) {
            if (getToken() != null && !getToken().equals("")) {//have token
//                loginBtn.setVisibility(View.GONE);
                logoutBtn.setVisibility(View.VISIBLE);
                Log.d("getToken: ", getToken());

            } else {// no token
//                loginBtn.setVisibility(View.VISIBLE);
                logoutBtn.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


    }

    public void setLoginLogoutBtn(ImageView loginBtn, ImageView logoutBtn) {
//        View myActionBar=this.findViewById(R.id.my_action_bar);
//        logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
//        loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        this.loginBtn = loginBtn;
        this.logoutBtn = logoutBtn;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    protected void onPause() {
        super.onPause();

    }


    public void goToLogin(View view) {

        if (loginBtn != null && logoutBtn != null) {
            if (getToken() != null && !getToken().equals("")) {//have token
                Intent intent = new Intent(this, DiarysActivity.class);
                startActivity(intent);
            } else {// no token
                Intent intent = new Intent(this, LoginActivity.class);

                startActivity(intent);
            }
        }


    }

    public void goToCheckList(View view) {
        Intent intent = new Intent(this, CheckListActivity.class);
        startActivity(intent);
    }

    public void setToken(String token) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("currentToken");
        editor.putString("currentToken", token);
        editor.commit();
    }

    public String getToken() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String auth_token_string = settings.getString("currentToken", ""/*default value*/);
        return auth_token_string;
    }

    private void removeToken() {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("currentToken");
        editor.commit();
    }

    public void goToLogout(View view) {
        removeToken();
        if (loginBtn != null && logoutBtn != null) {
//            loginBtn.setVisibility(View.VISIBLE);
            logoutBtn.setVisibility(View.GONE);
        }
    }
}
