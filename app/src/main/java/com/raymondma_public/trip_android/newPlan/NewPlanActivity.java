package com.raymondma_public.trip_android.newPlan;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;

public class NewPlanActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan);


        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_plan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
