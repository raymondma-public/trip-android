package com.raymondma_public.trip_android.planDetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class PlanDetailActivity extends BaseActivity {


//PlanParser
//    Response.Listener<JSONObject> eventResponseListener = new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//
//            event=eventParser.getOneEventFromEventJSONObj(response);
//
//
//            Picasso.with(TripApplication.getAppContext()).load(event.getRandomImgURL()).fit().into(mainImageView);
//
//            if(event!=null) {
//                titleTextView.setText(event.getTitle());
//                descriptionTV.setText(event.getDescription());
//            }
//
//        }
//    };
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_detail);

        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);

//        Intent intent=getIntent();
//        String id=intent.getStringExtra("id");
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.GET_PLAN+"?id="+id, planResponseListener, new ErrorToastListener());
//
//        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
