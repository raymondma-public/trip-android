package com.raymondma_public.trip_android;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by HEI on 14/10/2015.
 */
public class TripApplication extends Application{
    private static TripApplication singleton;
    private static Context mAppContext;
    public static TripApplication getInstance(){
        return singleton;

    }

    @Override
    public final void onCreate(){
        super.onCreate();
        singleton=this;

        Parse.initialize(this, "vo72H9qpcrZC2PE9z1oV11VWqW5C7lrk1Il41jk1", "DfLlgb2FgLVsd871nt1fHuOhIz2dZ6JTRbdSDodt");
        ParseInstallation.getCurrentInstallation().saveInBackground();
        Log.d("App", "onCreate");

        this.setAppContext(getApplicationContext());
    }


    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mAppContext) {
        this.mAppContext = mAppContext;
    }
}
