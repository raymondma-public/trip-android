package com.raymondma_public.trip_android.checklist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;

import java.util.ArrayList;

public class CheckListActivity extends BaseActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);


        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);


        ImageView checkListButton=(ImageView)myActionBar.findViewById(R.id.checkListButton);
        checkListButton.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) findViewById(R.id.checkListRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        ArrayList<String> myDataset=new ArrayList<>();
        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");


        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");
        mAdapter = new CheckListAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_check_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
