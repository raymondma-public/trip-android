package com.raymondma_public.trip_android.translate;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.utils.Translate;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;
import com.rmtheis.yandtran.ApiKeys;
import com.rmtheis.yandtran.language.Language;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

/**
 * Created by RaymondMa on 14/11/2015.
 */
public class TranslateActivity extends BaseActivity {

    private EditText txtSpeechInput;
    private EditText resultET;
    private ImageButton btnSpeak;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private TextToSpeech t1;

    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {


            try {
                resultET.setText(response.getString("data"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate);

        txtSpeechInput = (EditText) findViewById(R.id.txtSpeechInput);
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);
        resultET = (EditText) findViewById(R.id.resultET);
        // hide the action bar
        getSupportActionBar().hide();

        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.CHINESE);
                }
            }
        });

    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));
                    translateRequest();
//                    translate(result.get(0));
                }
                break;
            }

        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

//    private void translate(String text){
//        String sl="en";
//        String tl="zh-TW";
//
//        try {
//            String result=Translate.translate(sl, tl, text);
//            Log.d("Translate Result",result);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void speeakResult(View view) {

        String toSpeak = resultET.getText().toString();
        Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
        t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void translate(View view) {

        translateRequest();
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            String output=(String)msg.obj;
            resultET.setText(output);
        }
    };

    private void translateRequest() {
//        String source = "auto";
//        String target = "zh-TW";
//        String words = null;
//        try {
//            words = URLEncoder.encode(txtSpeechInput.getText().toString(), "utf-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        new Thread() {
            @Override
            public void run() {
                try {
                    Map<String, String> langs = null;

                    langs = TranslateAPI.getLangs();


                    String input = URLEncoder.encode(txtSpeechInput.getText().toString(), "utf-8");
                    ;

                    String source = TranslateAPI.detectLanguage(input);
                    String target = "zh";//TranslateAPI.getKey(langs, "Chinese");

                    String output = TranslateAPI.translate(input, source, target);

                    System.out.println("Detected lang: " + source + " (" + langs.get(source) + ")");

                    System.out.println("Input: " + input);
                    System.out.println("Output: " + output);

                    System.out.println("Source: " + source);
                    System.out.println("Target: " + target);

                    System.out.println("langs.get(source): " + langs.get(source));
                    System.out.println("langs.get(target): " + langs.get(target));
                    System.out.println("TranslateAPI.getKey(langs, \"Chinese\"): " + TranslateAPI.getKey(langs, "Chinese"));

                    Message msg=new Message();
                    msg.obj=output;
                    handler.sendMessage(msg);
//                    resultET.setText(output);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();



//
//
//
//        String wholeURL = Config.TRANSLATE_URL + "?sourceLanguage=" + source + "&targetLanguage=" + target + "&words=" + words;
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, wholeURL, responseListener, new ErrorToastListener());
////        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
//        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);
    }
}