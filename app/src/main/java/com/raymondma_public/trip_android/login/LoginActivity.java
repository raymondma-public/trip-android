package com.raymondma_public.trip_android.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.register.RegisterActivity;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.Toast.LENGTH_SHORT;


public class LoginActivity extends BaseActivity {


    private CallbackManager callbackManager;
//    private LoginButton loginButton;
    private EditText userNameEditText;
    private EditText passwordEditText;


    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            Toast.makeText(TripApplication.getAppContext(), "Login Successful", Toast.LENGTH_SHORT).show();
            JSONObject user= null;
            String finalToken="";
            try {
                user = response.getJSONObject("user");

                JSONArray tokenArr=user.getJSONArray("login_token");

                for(int i=0;i<tokenArr.length();i++){
                    try {
                        JSONObject tokenObj=tokenArr.getJSONObject(i);
                        finalToken=tokenObj.getString("token");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String token=finalToken;
            setToken(token);
            finish();
        }
    };
    //FB Callback
    private FacebookCallback<LoginResult> facebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Toast.makeText(LoginActivity.this, "Success", LENGTH_SHORT).show();

            // App code
        }

        @Override
        public void onCancel() {
            Toast.makeText(LoginActivity.this, "Cancel", LENGTH_SHORT).show();
            // App code
        }

        @Override
        public void onError(FacebookException exception) {
            Toast.makeText(LoginActivity.this, "Error", LENGTH_SHORT).show();
            // App code
        }
    };


    //FB login function
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


//        View myActionBar=findViewById(R.id.my_action_bar);
//        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
//        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
//        setLoginLogoutBtn(loginBtn, logoutBtn);

        //FB
        callbackManager = CallbackManager.Factory.create();
//        loginButton = (LoginButton) findViewById(R.id.facebookLoginBtn);
//        loginButton.registerCallback(callbackManager, facebookCallback);

        userNameEditText = (EditText) findViewById(R.id.userNameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToSignUp(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void onLogin(View view) {
        JSONObject loginData = new JSONObject();
        try {
            loginData.put("name", userNameEditText.getText().toString());
            loginData.put("password", passwordEditText.getText().toString());
            Log.d("name", userNameEditText.getText().toString());
            Log.d("password", passwordEditText.getText().toString());
        } catch (org.json.JSONException e) {

            e.printStackTrace();
        }

//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.GET_EVENT_LIST_URL, loginData, responseListener, new ErrorToastListener());
        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.POST, Config.LOGIN_URL, loginData, responseListener, new ErrorToastListener());
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);

    }
}
