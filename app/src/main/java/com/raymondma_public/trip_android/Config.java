package com.raymondma_public.trip_android;

/**
 * Created by HEI on 14/10/2015.
 */
public class Config {
    //    Base
    public static final String UBER_API = "https://api.uber.com/v1/products";
    public static final String SERVER_URL = "http://trip-002.herokuapp.com";
    //        public static final String SERVER_URL = "http://192.168.31.203:3000";
    public static final String SERVER_API_URL = SERVER_URL + "/api";

    //User
    public static final String CHECK_NAME_CAN_USE_URL = SERVER_API_URL + "/check-name-can-use";
    public static final String REGISTER_URL = SERVER_API_URL + "/register";
    public static final String LOGIN_URL = SERVER_API_URL + "/login";
    public static final String GET_USER = SERVER_API_URL + "/get-user";
    public static final String UPDATE_USER = SERVER_API_URL + "/update-user";


    //    PlanedEvent
    public static final String CREATE_PLANED_EVENT_URL = SERVER_API_URL + "/create-planed-event";
    public static final String GET_PLANED_EVENT_URL = SERVER_API_URL + "/get-planed-event";
    public static final String GET_PLANEDEVENT_LIST = SERVER_API_URL + "/get-planed-event-list";


//    Event

    public static final String CREATE_EVENT_URL = SERVER_API_URL + "/create-event";
    public static final String GET_EVENT_URL = SERVER_API_URL + "/get-event";

    public static final String GET_EVENT_LIST_URL = SERVER_API_URL + "/get-event-list";

    //    Plan
    public static final String CREATE_PLAN_URL = SERVER_API_URL + "/create-plan";
    public static final String GET_PLAN_LIST_URL_ = SERVER_API_URL + "/get-plan-list";
    public static final String GET_PLAN = SERVER_API_URL + "/get-plan";


    //    Blog
    public static final String CREATE_BLOG_URL = SERVER_API_URL + "/create-blog";
    public static final String GET_BLOG_URL = SERVER_API_URL + "/get-blog";
    public static final String GET_BLOG_LIST_URL = SERVER_API_URL + "/get-blog-list";


    //Hotel
    public static final String GET_NEAR_HOTEL_URL = SERVER_API_URL + "/hotel";

//    Translate

    public static final String TRANSLATE_URL = SERVER_API_URL + "/get-translate";
}
