package com.raymondma_public.trip_android.diaryMap;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.database.TaskDbHelper;
import com.raymondma_public.trip_android.database.TaskEntry;

import com.raymondma_public.trip_android.model.Thing;
import com.raymondma_public.trip_android.utils.TimeUtil;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class DiaryMapActivity extends BaseActivity {

    private static final String TAG = "MapActivity";
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    private TaskDbHelper dbHelper;
    private ArrayList<Thing> data = new ArrayList<>();

//    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//
//            Log.d(TAG, response.toString());
//            // Do a null check to confirm that we have not already instantiated the map.
//            if (mMap == null) {
//                // Try to obtain the map from the SupportMapFragment.
//                mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//                        .getMap();
//                // Check if we were successful in obtaining the map.
//                if (mMap != null) {
//                    setUpMap();
//                }
//            }
//
////                    .add(new LatLng(-33.866, 151.195))  // Sydney
////                    .add(new LatLng(-18.142, 178.431))  // Fiji
////                    .add(new LatLng(21.291, -157.821))  // Hawaii
////                    .add(new LatLng(37.423, -122.091)); // Mountain View
//            double latitude = 0;
//            double longitude = 0;
//
//            String toast = "";
////            try {
////
////                JSONArray snappedPoints = response.getJSONArray("snappedPoints");
////                for (int i = 0; i < snappedPoints.length(); i++) {
////                    JSONObject location = ((JSONObject) snappedPoints.get(i)).getJSONObject("location");//"location"
////                    latitude = location.getDouble("latitude");
////                    longitude = location.getDouble("longitude");
////                    list.add(new LatLng(latitude, longitude));
////                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Marker"));
////                    toast += latitude + " " + longitude + "\n";
////                }
////
////            } catch (JSONException e) {
////                e.printStackTrace();
////            }
//
//
//            try {
//
//                JSONArray routes = response.getJSONArray("routes");
//                for (int i = 0; i < routes.length(); i++) {
//                    PolylineOptions list = new PolylineOptions().geodesic(true);
//                    JSONArray legs = ((JSONObject) routes.get(i)).getJSONArray("legs");
//                    for (int j = 0; j < legs.length(); j++) {
//
//                        JSONObject start_location = ((JSONObject) legs.get(j)).getJSONObject("start_location");//"location"
//                        double start_lat = start_location.getDouble("lat");
//                        double start_lng = start_location.getDouble("lng");
//                        list.add(new LatLng(start_lat, start_lng));
//                        JSONObject end_location = ((JSONObject) legs.get(j)).getJSONObject("end_location");//"location"
//                        double end_lat = start_location.getDouble("lat");
//                        double end_lng = start_location.getDouble("lng");
//                        list.add(new LatLng(end_lat, end_lng));
//
//                        mMap.addMarker(new MarkerOptions().position(new LatLng(start_lat, start_lng)).title("Marker"));
////                    toast += start_lat + " " + start_lng + " "+end_lat+" "+end_lng+"\n";
//
//                        mMap.addPolyline(list);
//                    }
//                }
//
//                Toast.makeText(TripApplication.getAppContext(), routes.toString(), Toast.LENGTH_SHORT).show();
//
//
//                CameraUpdateFactory.newLatLngZoom(
//                        new LatLng(latitude, longitude), 10);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
//        setUpMapIfNeeded();

        dbHelper = new TaskDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {TaskEntry.COLUMN_NAME_TASK_ID,
                TaskEntry.COLUMN_NAME_TASK_TITLE,
                TaskEntry.COLUMN_NAME_TASK_START_LAT,
                TaskEntry.COLUMN_NAME_TASK_START_LONG,
                TaskEntry.COLUMN_NAME_TASK_END_LAT,
                TaskEntry.COLUMN_NAME_TASK_END_LONG,
                TaskEntry.COLUMN_NAME_TASK_TYPE,
                TaskEntry.COLUMN_NAME_TASK_FILE_PATH,
                TaskEntry.COLUMN_NAME_TASK_DATE_TIME};
        Intent intent = getIntent();


        int day = intent.getIntExtra("day", 0);
        int month = intent.getIntExtra("month", 0);
        int year = intent.getIntExtra("year", 0);

        final ArrayList<Long> tempData = new ArrayList();
        Cursor c = db.query(TaskEntry.TABLE_NAME, projection, null, null, TaskEntry.COLUMN_NAME_TASK_DATE_TIME, null, null);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(0);
                String title = c.getString(1);
                double startLat = c.getDouble(2);
                double startLong = c.getDouble(3);
                double endLat = c.getDouble(4);
                double endLong = c.getDouble(5);
                String type = c.getString(6);
                String filePath = c.getString(7);
                long timeStamp = c.getLong(8);


                if (isEqual(day, month, year, timeStamp)) {
                    tempData.add(timeStamp);
                    data.add(new Thing(id, title, startLat, startLong, endLat, endLong, type, filePath, timeStamp));
                }

            } while (c.moveToNext());
        }


        addNodes();
//        request();
    }

    private void addNodes() {

        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                LatLng lastLatLng = null;
                LatLng thisLatLng;
                int firstCounter = 0;
                ArrayList<Marker> myMarkers=new ArrayList();
                for (final Thing thing : data) {
                    Log.d("firstCounter", firstCounter + "");
                    double startLat = thing.getStartLat();
                    double startLong = thing.getStartLong();
                    Log.d("addNode", "addNode");
                    Log.d("startLat", "" + startLat);
                    Log.d("startLong", "" + startLong);
                    Log.d("thing.getType()", thing.getType());
                    thisLatLng = (new LatLng(startLat, startLong));
                    if (firstCounter > 0) {
                        mMap.addPolyline(new PolylineOptions().geodesic(false)
                                .add(thisLatLng)
                                .add(lastLatLng));  // Sydney

                    }


                    Marker myMarker = mMap.addMarker(new MarkerOptions().position(thisLatLng).title(thing.getType() ));//+ thing.getTimeStamp()
                    myMarker.showInfoWindow();
                    myMarkers.add(myMarker);

                    thing.setTempMarkerId(myMarker.getId());


                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                        @Override
                        public boolean onMarkerClick(Marker arg0) {

                            for (Thing thing1 : data) {

                                if (arg0.getId().equals(thing1.getTempMarkerId())) {
//                                if (arg0.getTitle().equals(thing1.getType() + thing1.getTimeStamp())) { // if marker source is clicked
                                    Toast.makeText(DiaryMapActivity.this, arg0.getTitle(), Toast.LENGTH_SHORT).show();// display toast
                                    Uri myUri = Uri.parse(thing1.getFilePath()); // initialize Uri here
                                    MediaPlayer mediaPlayer = new MediaPlayer();
                                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    try {
                                        mediaPlayer.setDataSource(getApplicationContext(), myUri);
                                        mediaPlayer.prepare(); // must call prepare first
                                        mediaPlayer.start(); // then start
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
// or just mediaPlayer.setDataSource(mFileName);

                            }
                            arg0.showInfoWindow();


                            return true;
                        }
                    });

                    lastLatLng = thisLatLng;
                    firstCounter++;
                }


//                LatLngBounds.Builder builder=new LatLngBounds.Builder();
//                for(Marker marker:myMarkers){
//                    builder.include(marker.getPosition());
//
//                }


//                LatLngBounds bounds=builder.build();
//                int padding=0;
//                CameraUpdate cu=CameraUpdateFactory.newLatLngBounds(bounds,padding);

                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 15));
            }
        }


//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                new LatLng(-18.142, 178.431), 2));

        // Polylines are useful for marking paths and routes on the map.
//        mMap.addPolyline(new PolylineOptions().geodesic(true)
//                .add(new LatLng(-33.866, 151.195))  // Sydney
//                .add(new LatLng(-18.142, 178.431))  // Fiji
//                .add(new LatLng(21.291, -157.821))  // Hawaii
//                .add(new LatLng(37.423, -122.091))  // Mountain View
//        );
    }

    private boolean isEqual(int day, int month, int year, long timeStamp) {

        boolean dayEqual = day == TimeUtil.getDayFromTimestamp(timeStamp);
        boolean monthEqual = month == TimeUtil.getMonthFromTimestamp(timeStamp);
        boolean yearEqual = year == TimeUtil.getYearFromTimestamp(timeStamp);
        return dayEqual && monthEqual && yearEqual;
    }


//    private void request() {
//
//        String requestURL = "https://maps.googleapis.com/maps/api/directions/json?origin=-35.28032,149.12907&destination=-35.28032,149.12907=optimize:true|Barossa+Valley,SA|Clare,SA|22.392321,114.109980|Connawarra,SA|McLaren+Vale,SA&key=AIzaSyB5wd0X7n1Y-luZLQ0JQlgh5fyhANDrbCw";
//
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, requestURL, responseListener, new ErrorToastListener());
////        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
//        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
//            if (mMap != null) {
//                setUpMap();
//            }
        }

//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                new LatLng(-18.142, 178.431), 2));
//
//        // Polylines are useful for marking paths and routes on the map.
//        mMap.addPolyline(new PolylineOptions().geodesic(true)
//                .add(new LatLng(-33.866, 151.195))  // Sydney
//                .add(new LatLng(-18.142, 178.431))  // Fiji
//                .add(new LatLng(21.291, -157.821))  // Hawaii
//                .add(new LatLng(37.423, -122.091))  // Mountain View
//        );
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */

//    private void setUpMap() {
//        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
//    }
}
