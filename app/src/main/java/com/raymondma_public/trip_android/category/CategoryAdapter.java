package com.raymondma_public.trip_android.category;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.eventlist.EventListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HEI on 16/10/2015.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    Activity activity;
    private ArrayList<String> mDataset;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CategoryAdapter(ArrayList<String> myDataset, Activity activity) {
        mDataset = myDataset;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        Picasso.with(TripApplication.getAppContext()).load("http://i3.sinaimg.cn/travel/photo/guilljfjq/U3325P704T93D1661F3923DT20090612155225.jpg").into(holder.imageViewLeft);
        Picasso.with(TripApplication.getAppContext()).load("http://i3.sinaimg.cn/travel/photo/guilljfjq/U3325P704T93D1661F3923DT20090612155225.jpg").into(holder.imageViewRight);

        holder.imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripApplication.getAppContext(), EventListActivity.class);
                activity.startActivity(intent);
            }
        });

        holder.imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripApplication.getAppContext(), EventListActivity.class);
                activity.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imageViewLeft;
        public ImageView imageViewRight;

        public ViewHolder(View v) {
            super(v);
            imageViewLeft = (ImageView) v.findViewById(R.id.categoryItemImageLeft);
            imageViewRight = (ImageView) v.findViewById(R.id.categoryItemImageRight);
        }
    }
}