package com.raymondma_public.trip_android.planlist;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.model.Plan;
import com.raymondma_public.trip_android.planDetail.PlanDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HEI on 16/10/2015.
 */
public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.ViewHolder> {
    Activity activity;
    private ArrayList<Plan> mDataset;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PlanListAdapter(ArrayList<Plan> myDataset, Activity activity) {
        mDataset = myDataset;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PlanListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.plan_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

//        Picasso.with(TripApplication.getAppContext()).load("http://i.imgur.com/DvpvklR.png").into(holder.imageViewLeft);

        Picasso.with(TripApplication.getAppContext()).load(mDataset.get(position).getRandomImg()).skipMemoryCache().into(holder.planImage);


        holder.planTitle.setText(mDataset.get(position).getTitle());
        holder.planContent.setText(mDataset.get(position).getDescription());
        holder.noOfDaysTV.setText(mDataset.get(position).getNumOfDays() + " Days");

        holder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripApplication.getAppContext(), PlanDetailActivity.class);
                intent.putExtra("id",mDataset.get(position).getId());
                activity.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView itemCard;
        TextView noOfDaysTV;
        TextView planTitle;
        ImageView planImage;
        TextView planContent;
        public ViewHolder(View v) {
            super(v);
            itemCard = (CardView) v.findViewById(R.id.eventItemCard);
            noOfDaysTV = (TextView) v.findViewById(R.id.noOfDaysTV);
            planTitle = (TextView) v.findViewById(R.id.planTitle);
            planImage = (ImageView) v.findViewById(R.id.planImage);
            planContent = (TextView) v.findViewById(R.id.planContent);
        }
    }
}