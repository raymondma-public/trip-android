package com.raymondma_public.trip_android.planlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.model.Blog;
import com.raymondma_public.trip_android.model.Plan;
import com.raymondma_public.trip_android.model.PlanedEvent;
import com.raymondma_public.trip_android.model.User;
import com.raymondma_public.trip_android.newPlan.NewPlanActivity;
import com.raymondma_public.trip_android.utils.JSONParser;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

public class PlanListActivity extends BaseActivity {


    FloatingActionButton fab;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String TAG = "PlanListActivity";
    private JSONParser jsonParser = JSONParser.getInstance();
    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());

//            Toast.makeText(TripApplication.getAppContext(),response.toString(), Toast.LENGTH_SHORT).show();
            ArrayList<Plan> myDataset = new ArrayList<>();
            JSONArray events;
            try {
                events = response.getJSONArray("data");

                for (int i = 0; i < events.length(); i++) {
                    JSONObject plan = (JSONObject) events.get(i);

                    String id = jsonParser.getStringWithNameFromJSON(plan, "_id");

                    String title = jsonParser.getStringWithNameFromJSON(plan, "title");
                    int numOfDays = jsonParser.getIntWithNameFromJSON(plan, "noOfDays");
                    ArrayList<PlanedEvent> planedEvents = null;
                    int rating = 0;
                    int viewed = 0;
                    ArrayList<Blog> blogs = null;
                    int createTime = 0;
                    User creator = null;

                    ArrayList<String> allImgURLs = jsonParser.getArrayListStringFromJsonArray(plan, "allImgURLs");

                    String description = jsonParser.getStringWithNameFromJSON(plan, "description");


                    myDataset.add(new Plan(id, numOfDays, title, planedEvents, rating, viewed, blogs, createTime, creator, allImgURLs, description));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            mAdapter = new PlanListAdapter(myDataset, PlanListActivity.this);

            SlideInBottomAnimationAdapter slideInBottomAnimationAdapter=new SlideInBottomAnimationAdapter(mAdapter);
            AlphaInAnimationAdapter alphaInAnimationAdapter=new AlphaInAnimationAdapter(slideInBottomAnimationAdapter);
            alphaInAnimationAdapter.setFirstOnly(false);

            mRecyclerView.setAdapter(alphaInAnimationAdapter);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_list);

        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);


        mRecyclerView = (RecyclerView) findViewById(R.id.planRecycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.GET_PLAN_LIST_URL_, responseListener, new ErrorToastListener());
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToNewPlan(View view) {
        Intent intent = new Intent(this, NewPlanActivity.class);
        startActivity(intent);
    }

}
