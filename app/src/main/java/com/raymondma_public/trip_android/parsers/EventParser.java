package com.raymondma_public.trip_android.parsers;

import com.raymondma_public.trip_android.model.Event;
import com.raymondma_public.trip_android.model.Location;
import com.raymondma_public.trip_android.model.User;
import com.raymondma_public.trip_android.utils.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class EventParser {
    private static EventParser instance = new EventParser();
    private JSONParser jsonParser = JSONParser.getInstance();

    private EventParser() {
    }

    public static EventParser getInstance() {
        return instance;
    }

    public ArrayList<Event> getEvenListFromResponse(JSONObject response) {

        ArrayList<Event> myDataset = new ArrayList<>();
        JSONArray events;
        try {
            events = response.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return myDataset;
        }
        for (int i = 0; i < events.length(); i++) {
            JSONObject event = getEventJsonFromEventsJsonArr(events, i);

            Event tempEvent = getOneEventFromEventJSONObj(event);
            myDataset.add(tempEvent);
        }

        return myDataset;
    }

    public Event getOneEventFromEventJSONObj(JSONObject event) {


        String id = jsonParser.getStringWithNameFromJSON(event, "_id");
        String title = jsonParser.getStringWithNameFromJSON(event, "title");
//            JSONArray eventTypeJSONArray;
        ArrayList<String> eventType = new ArrayList<>();
        eventType.addAll(getEventTypeFromEventJsonArr(event));


        int numPplWent = jsonParser.getIntWithNameFromJSON(event, "numPplWent");

        Location location = jsonParser.getLocationWithNAmeFromJSON(event, "location");
        String description = jsonParser.getStringWithNameFromJSON(event, "description");
        ArrayList<String> imgURLs = new ArrayList<>();
        imgURLs.addAll(getImgUrls(event));

        int stdStartTime = jsonParser.getIntWithNameFromJSON(event, "stdStartTime");
        int stdEndTime = jsonParser.getIntWithNameFromJSON(event, "stdEndTime");
        String phone = jsonParser.getStringWithNameFromJSON(event, "phone");
        String traffic = jsonParser.getStringWithNameFromJSON(event, "traffic");
        String tips = jsonParser.getStringWithNameFromJSON(event, "tips");
        int rating = jsonParser.getIntWithNameFromJSON(event, "rating");
        int viewed = jsonParser.getIntWithNameFromJSON(event, "viewed");
        int createTime = jsonParser.getIntWithNameFromJSON(event, "createTime");
        User creator = getCreator(event);

        return new Event(id, title, numPplWent, location, description, imgURLs, stdStartTime, stdEndTime, phone, traffic, tips, rating, viewed, createTime, creator);
    }

    private User getCreator(JSONObject event) {
        User result = null;
        try {
            String id = event.getString("createdBy");
            result = new User(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }


    private ArrayList<String> getImgUrls(JSONObject event) {
        ArrayList<String> result = new ArrayList<>();

        try {
            JSONArray imgs = event.getJSONArray("imgs");
            for (int i = 0; i < imgs.length(); i++) {
                result.add(imgs.getJSONObject(i).getString("srcURL"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result;
    }


//    private int getNoPplWent(JSONObject event) {
//        int  numPplWent = 0;
//        try {
//            numPplWent = event.getInt("numPplWent");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return numPplWent;
//    }

    private ArrayList<String> getEventTypeFromEventJsonArr(JSONObject event) {
        ArrayList<String> eventType = new ArrayList<>();
        JSONArray eventTypeJSONArray;
        try {
            eventTypeJSONArray = event.getJSONArray("eventType");

            for (int j = 0; j < eventTypeJSONArray.length(); j++) {
                try {
                    eventType.add(eventTypeJSONArray.getString(j));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return eventType;
    }

    private JSONObject getEventJsonFromEventsJsonArr(JSONArray events, int i) {
        JSONObject event = null;
        try {
            event = (JSONObject) events.get(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return event;
    }
//
//    private String getIdFromEventJSON(JSONObject event) {
//        String id = null;
//        try {
//            id = event.getString("_id");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return id;
//    }

}
