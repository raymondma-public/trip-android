package com.raymondma_public.trip_android.parsers;

import com.raymondma_public.trip_android.model.Blog;
import com.raymondma_public.trip_android.model.User;
import com.raymondma_public.trip_android.utils.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by RaymondMa on 9/11/2015.
 */
public class BlogParser {

    private JSONParser jsonParser = JSONParser.getInstance();
    private static BlogParser instance=new BlogParser();

    public static BlogParser getInstance(){
        return instance;
    }

    private BlogParser(){

    }

    public Blog getBlogWithNameFromJSON(JSONObject blog) {
        String id = jsonParser.getStringWithNameFromJSON(blog, "_id");
        String title = jsonParser.getStringWithNameFromJSON(blog, "title");
        String description = jsonParser.getStringWithNameFromJSON(blog, "description");
        int createTime = jsonParser.getIntWithNameFromJSON(blog, "createTime");
        User creator = new User(jsonParser.getStringWithNameFromJSON(blog, "createBy"));
        int viewed = jsonParser.getIntWithNameFromJSON(blog, "viewed");
        int rating = jsonParser.getIntWithNameFromJSON(blog, "rating");
//                    int imgURLs=jsonParser.getArrayListStringFromJsonArray(blog,"allImgURLs");
        ArrayList<String> imgURLs = new ArrayList<>();
        try {
            JSONArray imgURLsArr = blog.getJSONArray("imgs");

            for (int j = 0; j < imgURLsArr.length(); j++) {
                JSONObject imgObj = imgURLsArr.getJSONObject(j);
                imgURLs.add(jsonParser.getStringWithNameFromJSON(imgObj, "srcURL"));
            }

            return new Blog(id, title, description, createTime, creator, viewed, rating, imgURLs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
