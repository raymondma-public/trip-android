//package com.raymondma_public.trip_android.parsers;
//
//import com.raymondma_public.trip_android.model.Blog;
//import com.raymondma_public.trip_android.model.Event;
//import com.raymondma_public.trip_android.model.Location;
//import com.raymondma_public.trip_android.model.Plan;
//import com.raymondma_public.trip_android.model.PlanedEvent;
//import com.raymondma_public.trip_android.model.User;
//import com.raymondma_public.trip_android.utils.JSONParser;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
///**
// * Created by RaymondMa on 9/11/2015.
// */
//public class PlanParser {
//
//
//    private static PlanParser instance = new PlanParser();
//    private JSONParser jsonParser = JSONParser.getInstance();
//
//    private PlanParser() {
//    }
//
//    public static PlanParser getInstance() {
//        return instance;
//    }
//
//    public ArrayList<Plan> getPlanListFromResponse(JSONObject response) {
//
//        ArrayList<Plan> myDataset = new ArrayList<>();
//        JSONArray plans;
//        try {
//            plans = response.getJSONArray("data");
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return myDataset;
//        }
//        for (int i = 0; i < plans.length(); i++) {
//            JSONObject plan = getPlanJsonFromPlansJsonArr(plans, i);
//
//            Plan tempEvent = getOnePlanFromPlanJSONObj(plan);
//            myDataset.add(tempEvent);
//        }
//
//        return myDataset;
//    }
//
//    private Plan getOnePlanFromPlanJSONObj(JSONObject plan) {
//
//        String id = jsonParser.getStringWithNameFromJSON(plan, "_id");
//        String title = jsonParser.getStringWithNameFromJSON(plan, "title");
//        int numOfDays = jsonParser.getIntWithNameFromJSON(plan, "noOfDays");
//
//        JSONArray planedEventsJSONArr=jsonParser.getJsonArrayWithNameFromJSON(plan, "planedEvents");
//
//        ArrayList<PlanedEvent> planedEvents = new ArrayList<>();
//        for(int i=0;i<planedEventsJSONArr.length();i++){
//            planedEvents.add(jsonParser);
//        }
//
//
//        int rating = jsonParser.getIntWithNameFromJSON(plan,"rating");
//        int viewed = jsonParser.getIntWithNameFromJSON(plan,"viewed");
//        ArrayList<Blog> blogs = null;
//        int createTime = jsonParser.getIntWithNameFromJSON(plan,"createTime");
//        User creator = jsonParser.getUserWithNameFromJSON(plan,"createdBy");
//
//
//
//
//
//
//
////            JSONArray eventTypeJSONArray;
//        ArrayList<String> planType = new ArrayList<>();
//        planType.addAll(getPlanTypeFromPlanJsonArr(plan));
//
//
//
//        return new Plan();
//    }
//
//    private User getCreator(JSONObject event) {
//        User result = null;
//        try {
//            String id = event.getString("createdBy");
//            result = new User(id);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//
//    private ArrayList<String> getImgUrls(JSONObject event) {
//        ArrayList<String> result = new ArrayList<>();
//
//        try {
//            JSONArray imgs = event.getJSONArray("imgs");
//            for (int i = 0; i < imgs.length(); i++) {
//                result.add(imgs.getJSONObject(i).getString("srcURL"));
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        return result;
//    }
//
//
////    private int getNoPplWent(JSONObject event) {
////        int  numPplWent = 0;
////        try {
////            numPplWent = event.getInt("numPplWent");
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////        return numPplWent;
////    }
//
////    private ArrayList<String> getPlanTypeFromPlanJsonArr(JSONObject plan) {
////        ArrayList<String> planType = new ArrayList<>();
////        JSONArray eventTypeJSONArray;
////        try {
////            eventTypeJSONArray = plan.getJSONArray("eventType");
////
////            for (int j = 0; j < eventTypeJSONArray.length(); j++) {
////                try {
////                    planType.add(eventTypeJSONArray.getString(j));
////                } catch (JSONException e) {
////                    e.printStackTrace();
////                }
////            }
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////        return planType;
////    }
//
//    private JSONObject getPlanJsonFromPlansJsonArr(JSONArray plans, int i) {
//        JSONObject plan = null;
//        try {
//            plan = (JSONObject) plans.get(i);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return plan;
//    }
//
//
//}
