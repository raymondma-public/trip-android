package com.raymondma_public.trip_android.myPlan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.newPlan.NewPlanActivity;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

public class MyPlanActivity extends BaseActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_plan);

        View myActionBar=findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);

        mRecyclerView = (RecyclerView) findViewById(R.id.myPlanRecycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ArrayList<String> myDataset = new ArrayList<>();
        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");


        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");

        myDataset.add("Money");
        myDataset.add("Cloth");
        myDataset.add("Mobile");
        myDataset.add("Laptop");
        myDataset.add("Flight ticket");
        myDataset.add("Book Hotel");
        myDataset.add("Book Hotel");
        mAdapter = new MyPlanListAdapter(myDataset, this);

        SlideInBottomAnimationAdapter slideInBottomAnimationAdapter=new SlideInBottomAnimationAdapter(mAdapter);
        AlphaInAnimationAdapter alphaInAnimationAdapter=new AlphaInAnimationAdapter(slideInBottomAnimationAdapter);
        alphaInAnimationAdapter.setFirstOnly(false);

        mRecyclerView.setAdapter(alphaInAnimationAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_plan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToNewPlan(View view) {
        Intent intent = new Intent(this, NewPlanActivity.class);
        startActivity(intent);
    }
}
