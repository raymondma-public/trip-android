package com.raymondma_public.trip_android.model;

import java.util.ArrayList;

/**
 * Created by HEI on 15/10/2015.
 */
public class Blog {
    //compulsory
    private String id;
    private String title ;
    private String description ;
    private ArrayList<String> imgURLs = new ArrayList<>();
    private int createTime;
    private User creator;
    private int viewed;
    private int rating;
    //optional


    public Blog(String id, String title, String description, int createTime, User creator, int viewed, int rating,ArrayList<String> imgURLs) {
        super();
        this.id = id;

        this.title = title;
        this.description = description;
        this.createTime = createTime;
        this.creator = creator;
        this.viewed = viewed;
        this.rating = rating;
        this.imgURLs=imgURLs;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getRandomImgURL() {
        int index = (int) Math.random() * imgURLs.size();

        if(imgURLs.size()==0){
            return null;
        }

        return imgURLs.get(index);
    }

    public String getId() {
        return id;
    }
}
