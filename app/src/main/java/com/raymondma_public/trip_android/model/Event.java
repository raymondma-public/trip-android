package com.raymondma_public.trip_android.model;


import java.util.ArrayList;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class Event {
    //compulsory
    private String id;
    private String title;


//    optional

    private ArrayList<String> eventType = new ArrayList<>();
    private int numPplWent;
    private Location location;
    private String description;
    private ArrayList<String> imgURLs = new ArrayList<>();
    private int stdStartTime;
    private int stdEndTime;
    private String phone;
    private String traffic;
    private String tips;
    private int rating;
    private int viewed;
    private int createTime;
    private User creator;


    public Event(String id, String title, int numPplWent, Location location, String description, ArrayList<String> imgURLs, int stdStartTime, int stdEndTime, String phone, String traffic, String tips, int rating, int viewed, int createTime, User creator) {
        this.id = id;
        this.title = title;
        this.numPplWent = numPplWent;
        this.location = location;
        this.description = description;
        this.imgURLs = imgURLs;
        this.stdStartTime = stdStartTime;
        this.stdEndTime = stdEndTime;
        this.phone = phone;
        this.traffic = traffic;

        this.tips = tips;
        this.rating = rating;
        this.viewed = viewed;
        this.createTime = createTime;
        this.creator = creator;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<String> getImgURLs() {
        return imgURLs;
    }

    public String getRandomImgURL() {
        int index = (int) Math.random() * imgURLs.size();
        if(imgURLs.size()==0){
            return null;
        }
        return imgURLs.get(index);
    }

    public String getID() {
        return id;
    }

    public Location getLocation() {
        return location;
    }
}
