package com.raymondma_public.trip_android.model;

import java.util.ArrayList;

/**
 * Created by RaymondMa on 4/11/2015.
 */
public class Plan {
    private String id;
    private int numOfDays;
    private String title;
    private ArrayList<PlanedEvent> planedEvents = new ArrayList<>();
    private int rating;
    private int viewed;
    private ArrayList<Blog> blogs = new ArrayList<>();
    private int createTime;
    private User creator;
    private ArrayList<String> allImageURLs = new ArrayList<>();

    private String description;


    public Plan(String id, int numOfDays, String title, ArrayList<PlanedEvent> planedEvents, int rating, int viewed, ArrayList<Blog> blogs, int createTime, User creator, ArrayList<String> allImageURLs, String description) {
        this.id = id;
        this.numOfDays = numOfDays;
        this.title = title;
        this.planedEvents = planedEvents;
        this.rating = rating;
        this.viewed = viewed;
        this.blogs = blogs;
        this.createTime = createTime;
        this.creator = creator;
        this.allImageURLs = allImageURLs;
        this.description = description;
    }

    public String getId() {
        return id;
    }


    public int getNumOfDays() {
        return numOfDays;
    }

    public String getRandomImg() {
        int index = (int) Math.random() * allImageURLs.size();
        if (allImageURLs.size() == 0) {
            return null;
        }
        return allImageURLs.get(index);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}

