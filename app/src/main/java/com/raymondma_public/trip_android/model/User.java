package com.raymondma_public.trip_android.model;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class User {
    private String id;
    private String loginToken;
    private String phoneNumber;
    private String user_name;

    public User(String id, String loginToken, String phoneNumber, String user_name) {
        this.id = id;
        this.loginToken = loginToken;
        this.phoneNumber = phoneNumber;
        this.user_name = user_name;
    }

    public User(String id) {
        this.id = id;
    }
}
