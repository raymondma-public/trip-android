package com.raymondma_public.trip_android.model;

import com.raymondma_public.trip_android.utils.TimeUtil;

/**
 * Created by RaymondMa on 15/11/2015.
 */
public class Thing {

    private int id;
    private String title;
    private double startLat;
    private double startLong;
    private double endLat;
    private double endLong;
    private String type;
    private String filePath;
    private Long timeStamp;
    private String tempMarkerId;

    public Thing(int id, String title, double startLat, double startLong, double endLat, double endLong, String type, String filePath, Long timeStamp) {
        this.id = id;
        this.title = title;
        this.startLat = startLat;
        this.startLong = startLong;
        this.endLat = endLat;
        this.endLong = endLong;
        this.type = type;
        this.filePath = filePath;
        this.timeStamp = timeStamp;


    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getStartLat() {
        return startLat;
    }

    public double getStartLong() {
        return startLong;
    }

    public double getEndLat() {
        return endLat;
    }

    public double getEndLong() {
        return endLong;
    }

    public String getType() {
        return type;
    }

    public String getFilePath() {
        return filePath;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public int getDay() {
        return TimeUtil.getDayFromTimestamp(timeStamp);
    }

    public int getMonth() {
        return TimeUtil.getMonthFromTimestamp(timeStamp);
    }

    public int getYear() {
        return TimeUtil.getYearFromTimestamp(timeStamp);
    }

    public String getTempMarkerId() {
        return tempMarkerId;
    }

    public void setTempMarkerId(String tempMarkerId) {
        this.tempMarkerId = tempMarkerId;
    }
}
