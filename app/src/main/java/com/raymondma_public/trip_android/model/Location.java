package com.raymondma_public.trip_android.model;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class Location {
    private String locationName;
    private double latitude;
    private double longitude;

    public Location(String locationName, double latitude, double longitude) {
        this.locationName = locationName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
