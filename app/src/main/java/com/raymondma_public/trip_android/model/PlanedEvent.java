package com.raymondma_public.trip_android.model;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class PlanedEvent {

    private String id;
    private PlanedEvent event;
    private int desigedStartTime;
    private int designedEndTime;


    public PlanedEvent(String id, PlanedEvent event, int desigedStartTime, int designedEndTime) {
        this.id = id;
        this.event = event;
        this.desigedStartTime = desigedStartTime;
        this.designedEndTime = designedEndTime;
    }
}
