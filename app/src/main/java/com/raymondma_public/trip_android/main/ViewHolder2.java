package com.raymondma_public.trip_android.main;

import android.support.v7.widget.RecyclerView;
import android.telecom.TelecomManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.raymondma_public.trip_android.R;

/**
 * Created by HEI on 15/10/2015.
 */
public class ViewHolder2 extends RecyclerView.ViewHolder {


    private ImageView blogImage;
    private TextView titleTV;
    private TextView descriptionTV;
    private LinearLayout blogItemLayout;
    public ViewHolder2(View v) {
        super(v);
        blogImage = (ImageView) v.findViewById(R.id.blogImage);
        titleTV=(TextView)v.findViewById(R.id.titleTV);
        descriptionTV=(TextView)v.findViewById(R.id.descriptionTV);
        blogItemLayout=(LinearLayout)v.findViewById(R.id.blogItemLayout);
    }

    public ImageView getImageView() {
        return blogImage;
    }


    public TextView getTitleTV() {
        return titleTV;
    }


    public TextView getDescriptionTV() {
        return descriptionTV;
    }

    public View getBlogItemLayout() {
        return blogItemLayout;
    }
}