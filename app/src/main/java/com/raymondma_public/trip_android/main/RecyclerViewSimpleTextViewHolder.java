package com.raymondma_public.trip_android.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.raymondma_public.trip_android.R;

/**
 * Created by HEI on 15/10/2015.
 */
public class RecyclerViewSimpleTextViewHolder extends RecyclerView.ViewHolder {

    private TextView simpleTextView;

    public RecyclerViewSimpleTextViewHolder(View v) {
        super(v);

        simpleTextView=(TextView)v.findViewById(R.id.simpleText);
    }

    public TextView getLabel() {
        return simpleTextView;
    }
}
