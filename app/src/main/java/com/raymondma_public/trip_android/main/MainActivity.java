package com.raymondma_public.trip_android.main;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.camera2.*;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.category.CategoryActivity;
import com.raymondma_public.trip_android.community.CommunityActivity;
import com.raymondma_public.trip_android.database.TaskDbHelper;
import com.raymondma_public.trip_android.database.TaskEntry;
import com.raymondma_public.trip_android.eventDetail.EventDetailActivity;
import com.raymondma_public.trip_android.eventlist.EventListActivity;
import com.raymondma_public.trip_android.location.SingleShotLocationProvider;
import com.raymondma_public.trip_android.model.Blog;
import com.raymondma_public.trip_android.model.Location;
import com.raymondma_public.trip_android.model.Plan;
import com.raymondma_public.trip_android.model.PlanedEvent;
import com.raymondma_public.trip_android.model.User;
import com.raymondma_public.trip_android.myPlan.MyPlanActivity;
import com.raymondma_public.trip_android.parsers.BlogParser;
import com.raymondma_public.trip_android.planlist.PlanListActivity;
import com.raymondma_public.trip_android.planlist.PlanListAdapter;
import com.raymondma_public.trip_android.text.NewTextActivity;
import com.raymondma_public.trip_android.translate.TranslateActivity;
import com.raymondma_public.trip_android.utils.AudioRecorder;
import com.raymondma_public.trip_android.utils.CameraRecorder;
import com.raymondma_public.trip_android.utils.JSONParser;
import com.raymondma_public.trip_android.utils.MyService;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;
import com.squareup.picasso.Picasso;
import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;
import com.txusballesteros.bubbles.OnInitializedCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

//@SuppressWarnings("deprecation")
public class MainActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener //, SurfaceHolder.Callback
{

    RecyclerView recyclerView;
    //toolBar
//    private Toolbar mToolbar;
    //    Slider
//    private SliderLayout mDemoSlider;
    //    Bubble
    private BubblesManager bubblesManager;

    private String TAG = "MainActivity";
    private JSONParser jsonParser = JSONParser.getInstance();
    private BlogParser blogParser = BlogParser.getInstance();

//    private ImageView firstBlog;
//    private ImageView secondBlog;
//    private ImageView thirdBlog;


    private Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());


            HashMap<String, String> url_maps = new HashMap<String, String>();

            ArrayList<String> bannerImageArrayList = new ArrayList<>();
            try {
                JSONArray bannerImages = response.getJSONArray("bannerImages");
                for (int i = 0; i < bannerImages.length(); i++) {
                    String tempImgURL = bannerImages.getString(i);
                    bannerImageArrayList.add(tempImgURL);
//                    url_maps.put(""+i, tempImgURL);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            url_maps.put("Hannibal", "http://ad.qyer.com/www/images/2776e31c528623580b6284a5e7026265.jpg");
////        url_maps.put("Big Bang Theory", "http://ad.qyer.com/www/images/2776e31c528623580b6284a5e7026265.jpg");
//            url_maps.put("House of Cards", "http://ad.qyer.com/www/images/03673b84c256832d5980e59e6456a48d.jpg");
//            url_maps.put("Game of Thrones", "http://ad.qyer.com/www/images/bbfbaa44c695f9d6af4e631b9b4cba85.jpg");


            ArrayList<Blog> topBlogs = new ArrayList<>();


            try {
                JSONArray top3Blogs = response.getJSONArray("top3Blogs");
                for (int i = 0; i < top3Blogs.length(); i++) {
                    JSONObject jsonObject = top3Blogs.getJSONObject(i);

                    Blog blog = blogParser.getBlogWithNameFromJSON(jsonObject);
                    topBlogs.add(blog);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            ArrayList<Object> items = new ArrayList<>();
            items.add("image");


            JSONArray blogs;
            try {
                blogs = response.getJSONArray("data");
                for (int i = 0; i < blogs.length(); i++) {

                    JSONObject blog = (JSONObject) blogs.get(i);


                    Blog blogObj = blogParser.getBlogWithNameFromJSON(blog);
                    items.add(blogObj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            bindDataToAdapter(items, topBlogs, bannerImageArrayList);

//            Toast.makeText(TripApplication.getAppContext(),response.toString(), Toast.LENGTH_SHORT).show();
//            ArrayList<Plan> myDataset = new ArrayList<>();


        }
    };
    private TaskDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        FB
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main);
        View myActionBar = findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);


//         top 3 blogs
//        firstBlog=(ImageView)findViewById(R.id.firstBlog);
//        secondBlog=(ImageView)findViewById(R.id.secondBlog);
//        thirdBlog=(ImageView)findViewById(R.id.thirdBlog);

//        SlideShow
//        mDemoSlider = (SliderLayout) findViewById(R.id.slider);


//Bubble
        initializeBubblesManager();


        recyclerView = (RecyclerView) findViewById(R.id.complexRecyclerView);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<Object> items = new ArrayList<>();
        items.add("image");
        ArrayList<Objects> topBlogs = new ArrayList<>();
        ArrayList<String> bannerImages = new ArrayList<>();
        bindDataToAdapter(items, topBlogs, bannerImages);

        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.GET_BLOG_LIST_URL, responseListener, new ErrorToastListener());
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

//Slidee show
//s


        return super.onOptionsItemSelected(item);
    }


    //    FB function (put in default Activity)
    @Override
    protected void onResume() {
        super.onResume();


        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }


    //    FB function (put in default Activity)
    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }


    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
//        Slide show
//        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
//        Slide show
        Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
    }

    //Slide show
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }


    //Slide show
    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    //Slide show
    @Override
    public void onPageScrollStateChanged(int state) {
    }


    public boolean isRecording = false;
    AudioRecorder recorder;

    private String mFileName;
    private MediaRecorder mediaRecorder;


    private static final int VIDEO_CAPTURE = 101;
    Uri videoUri;

    public void startRecordingVideo() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Long tsLong = System.currentTimeMillis() / 1000;
            String currentTimeStamp = tsLong.toString();

            File mediaFile = new File(
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + currentTimeStamp + ".mp4");
            videoUri = Uri.fromFile(mediaFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
            startActivityForResult(intent, VIDEO_CAPTURE);
        } else {
            Toast.makeText(this, "No camera on device", Toast.LENGTH_LONG).show();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Video has been saved to:\n" + data.getData(), Toast.LENGTH_LONG).show();
                playbackRecordedVideo("" + data.getData());

                SingleShotLocationProvider.requestSingleUpdate(TripApplication.getAppContext(),
                        new SingleShotLocationProvider.LocationCallback() {
                            @Override
                            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                Log.d("Location", "my location is " + location.toString());
//                                startLocation = new Location("Start Location", location.longitude, location.latitude);


//                                insertVideo(location.longitude, location.latitude, "" + data.getData());


                            }
                        });
            } else if (resultCode == RESULT_CANCELED) {
//                Toast.makeText(this, "Video recording cancelled.", Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(this, "Failed to record video", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void playbackRecordedVideo(String newVideoPath) {
//        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
//        mVideoView.setVideoURI(videoUri);
//        mVideoView.setMediaController(new android.widget.MediaController(MainActivity.this));
//        mVideoView.requestFocus();
//        mVideoView.start();


        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newVideoPath));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.parse(newVideoPath), "video/mp4");
        startActivity(intent);
    }

    private Intent translateIntent;


    private boolean menuOpen = false;
    private Location startLocation;
    private Location endLocation;


    private void addNewBubbleMenu(final String type) {


        final BubbleLayout bubbleView = (BubbleLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.bubble_menu_layout, null);

        bubbleView.setOnBubbleRemoveListener(new BubbleLayout.OnBubbleRemoveListener() {
            @Override
            public void onBubbleRemoved(BubbleLayout bubble) {
            }
        });


        bubbleView.setOnBubbleClickListener(new BubbleLayout.OnBubbleClickListener() {

            @Override
            public void onBubbleClick(final BubbleLayout bubble) {


                bubble.findViewById(R.id.avatar1).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ImageView avatar = (ImageView) bubbleView.findViewById(R.id.avatar);
                        Resources resources = getResources();
                        Drawable drawable = ContextCompat.getDrawable(TripApplication.getAppContext(), R.drawable.circle_audio_record_normal_btn);

//                        avatar.setImageDrawable(drawable);
                        menuOpen = true;


                        if (!isRecording) {

                            startLocation = null;
                            endLocation = null;
                            mFileName = null;

                            SingleShotLocationProvider.requestSingleUpdate(TripApplication.getAppContext(),
                                    new SingleShotLocationProvider.LocationCallback() {
                                        @Override
                                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                            Log.d("Location", "my location is " + location.toString());
                                            startLocation = new Location("Start tLocation", location.longitude, location.latitude);


                                            insertThing();


                                        }
                                    });

                            PackageManager pmanager = getPackageManager();
                            if (pmanager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
                                // Set the file location for the audio
                                mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
                                Long tsLong = System.currentTimeMillis() / 1000;
                                String currentTimeStamp = tsLong.toString();
                                mFileName += "/" + currentTimeStamp + ".3gp";

                                Log.d("mFileName", mFileName);

                                // Create the recorder
                                mediaRecorder = new MediaRecorder();
                                // Set the audio format and encoder
                                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                                mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                                // Setup the output location
                                mediaRecorder.setOutputFile(mFileName);
                                // Start the recording
                                try {
                                    mediaRecorder.prepare();
                                    mediaRecorder.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            } else { // no mic on device
                                Toast.makeText(TripApplication.getAppContext(), "This device doesn't have a mic!", Toast.LENGTH_LONG).show();
                            }

                            isRecording = true;
                            ImageView avatarIV = (ImageView) bubble.findViewById(R.id.avatar1);


                            drawable = ContextCompat.getDrawable(TripApplication.getAppContext(), R.drawable.circle_audio_recording_btn);

                            avatarIV.setImageDrawable(drawable);


                        } else {


                            SingleShotLocationProvider.requestSingleUpdate(TripApplication.getAppContext(),
                                    new SingleShotLocationProvider.LocationCallback() {
                                        @Override
                                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                            Log.d("Location", "my location is " + location.toString());
                                            endLocation = new Location("End tLocation", location.longitude, location.latitude);
                                            insertThing();

                                        }
                                    });


                            mediaRecorder.stop();
                            mediaRecorder.reset();
                            mediaRecorder.release();
                            isRecording = false;
                            ImageView avatarIV = (ImageView) bubble.findViewById(R.id.avatar1);


                            drawable = ContextCompat.getDrawable(TripApplication.getAppContext(), R.drawable.circle_audio_record_normal_btn);

                            avatarIV.setImageDrawable(drawable);

                            MediaPlayer mediaPlayer = new MediaPlayer();
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                            try {
                                mediaPlayer.setDataSource(mFileName);
                                mediaPlayer.prepare(); // must call prepare first
                                mediaPlayer.start(); // then start
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });


                bubble.findViewById(R.id.avatar2).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startRecordingVideo();
                        closeBubbleMenu(bubble);
                    }
                });

                bubble.findViewById(R.id.avatar3).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, TranslateActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        startActivity(intent);
                        closeBubbleMenu(bubble);
                    }
                });


                bubble.findViewById(R.id.avatar4).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startText();
                        closeBubbleMenu(bubble);
                    }
                });

                if (menuOpen) {
                    menuOpen = false;
                    closeBubbleMenu(bubble);
                } else {
                    menuOpen = true;
                    openBubbleMenu(bubble);
                }


            }
        });


        bubbleView.setShouldStickToWall(true);

        ImageView avatarIV = (ImageView) bubbleView.findViewById(R.id.avatar);

        bubblesManager.addBubble(bubbleView, 60, 20);
    }


    private void insertVideo(double lat, double lng, String filepath) {

        Log.d("insertVideo", "insertVideo");


//            String toast = "startLat: " + startLocation.getLatitude() + "\n" +
//                    "startLong: " + startLocation.getLongitude() + "\n" +
//                    "endLat: " + endLocation.getLatitude() + "\n" +
//                    "endLong: " + endLocation.getLongitude() + "\n" +
//                    "filePath: " + mFileName;
//            Toast.makeText(TripApplication.getAppContext(), toast, Toast.LENGTH_LONG).show();

        dbHelper = new TaskDbHelper(MainActivity.this);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskEntry.COLUMN_NAME_TASK_ID, 1);
        values.put(TaskEntry.COLUMN_NAME_TASK_TITLE, "Title");

        values.put(TaskEntry.COLUMN_NAME_TASK_START_LAT, lat);
        values.put(TaskEntry.COLUMN_NAME_TASK_START_LONG, lng);
//            values.put(TaskEntry.COLUMN_NAME_TASK_END_LAT, endLocation.getLatitude());
//            values.put(TaskEntry.COLUMN_NAME_TASK_END_LONG, endLocation.getLongitude());

        values.put(TaskEntry.COLUMN_NAME_TASK_TYPE, "VIDEO");
        values.put(TaskEntry.COLUMN_NAME_TASK_FILE_PATH, filepath);
        Long tsLong = System.currentTimeMillis();
        String timestamp = tsLong.toString();
        values.put(TaskEntry.COLUMN_NAME_TASK_DATE_TIME, timestamp);
        db.insert(TaskEntry.TABLE_NAME, null, values);

    }

    private void insertThing() {

        Log.d("insertThing", "insertThing");
        if (startLocation != null && endLocation != null) {

            String toast =
                    "Saving Audio:\n" +
                            "startLat: " + startLocation.getLatitude() + "\n" +
                            "startLong: " + startLocation.getLongitude() + "\n" +
                            "endLat: " + endLocation.getLatitude() + "\n" +
                            "endLong: " + endLocation.getLongitude() + "\n" +
                            "filePath: " + mFileName;
            Toast.makeText(TripApplication.getAppContext(), toast, Toast.LENGTH_LONG).show();

            dbHelper = new TaskDbHelper(MainActivity.this);

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(TaskEntry.COLUMN_NAME_TASK_ID, 1);
            values.put(TaskEntry.COLUMN_NAME_TASK_TITLE, "Title");

            values.put(TaskEntry.COLUMN_NAME_TASK_START_LAT, startLocation.getLatitude());
            values.put(TaskEntry.COLUMN_NAME_TASK_START_LONG, startLocation.getLongitude());
            values.put(TaskEntry.COLUMN_NAME_TASK_END_LAT, endLocation.getLatitude());
            values.put(TaskEntry.COLUMN_NAME_TASK_END_LONG, endLocation.getLongitude());

            values.put(TaskEntry.COLUMN_NAME_TASK_TYPE, "AUDIO");
            values.put(TaskEntry.COLUMN_NAME_TASK_FILE_PATH, mFileName);
            Long tsLong = System.currentTimeMillis();
            String timestamp = tsLong.toString();
            values.put(TaskEntry.COLUMN_NAME_TASK_DATE_TIME, timestamp);
            db.insert(TaskEntry.TABLE_NAME, null, values);
        }
    }

    private void closeBubbleMenu(BubbleLayout bubble) {
        bubble.findViewById(R.id.bubbleItems).setVisibility(View.GONE);
        bubble.findViewById(R.id.avatar).setVisibility(View.VISIBLE);
    }

    private void openBubbleMenu(BubbleLayout bubble) {
        bubble.findViewById(R.id.bubbleItems).setVisibility(View.VISIBLE);
        bubble.findViewById(R.id.avatar).setVisibility(View.GONE);
    }

    private void startText() {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void initializeBubblesManager() {
        bubblesManager = new BubblesManager.Builder(this)
                .setTrashLayout(R.layout.bubble_trash_layout)
                .setInitializationCallback(new OnInitializedCallback() {
                    @Override
                    public void onInitialized() {
//                        addNewBubble();
                    }
                })
                .build();
        bubblesManager.initialize();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bubblesManager.recycle();
    }

    public void goToEventDetail(View view) {
        Intent intent = new Intent(this, EventDetailActivity.class);

        startActivity(intent);
    }


    private void bindDataToAdapter(ArrayList list, ArrayList topData, ArrayList bannerImages) {
        // Bind adapter to recycler view object

        ComplexRecyclerViewAdapter complexRecyclerViewAdapter = new ComplexRecyclerViewAdapter(list, topData, bannerImages);
        SlideInBottomAnimationAdapter slideInBottomAnimationAdapter = new SlideInBottomAnimationAdapter(complexRecyclerViewAdapter);
        AlphaInAnimationAdapter alphaInAnimationAdapter = new AlphaInAnimationAdapter(slideInBottomAnimationAdapter);
        recyclerView.setAdapter(alphaInAnimationAdapter);
        complexRecyclerViewAdapter.notifyDataSetChanged();

    }

    public void goToMyPlan(View view) {
        Intent intent = new Intent(MainActivity.this, TranslateActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public void goToEventList(View view) {
        Intent intent = new Intent(this, EventListActivity.class);
        startActivity(intent);
    }

    public void goToPlansList(View view) {
//        Intent intent = new Intent(this, PlanListActivity.class);
//        startActivity(intent);

        if (!isRecording) {

            startLocation = null;
            endLocation = null;
            mFileName = null;

            SingleShotLocationProvider.requestSingleUpdate(TripApplication.getAppContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.d("Location", "my location is " + location.toString());
                            startLocation = new Location("Start tLocation", location.longitude, location.latitude);


                            insertThing();


                        }
                    });

            PackageManager pmanager = getPackageManager();
            if (pmanager.hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
                // Set the file location for the audio
                mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
                Long tsLong = System.currentTimeMillis() / 1000;
                String currentTimeStamp = tsLong.toString();
                mFileName += "/" + currentTimeStamp + ".3gp";

                Log.d("mFileName", mFileName);

                // Create the recorder
                mediaRecorder = new MediaRecorder();
                // Set the audio format and encoder
                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                // Setup the output location
                mediaRecorder.setOutputFile(mFileName);
                // Start the recording
                try {
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else { // no mic on device
                Toast.makeText(TripApplication.getAppContext(), "This device doesn't have a mic!", Toast.LENGTH_LONG).show();
            }

            isRecording = true;


            ImageView avatarIV = (ImageView) findViewById(R.id.recordBtn);


            Drawable drawable = ContextCompat.getDrawable(TripApplication.getAppContext(), R.drawable.circle_audio_recording_btn);

            avatarIV.setImageDrawable(drawable);


        } else {


            SingleShotLocationProvider.requestSingleUpdate(TripApplication.getAppContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.d("Location", "my location is " + location.toString());
                            endLocation = new Location("End tLocation", location.longitude, location.latitude);
                            insertThing();

                        }
                    });


            mediaRecorder.stop();
            mediaRecorder.reset();
            mediaRecorder.release();
            isRecording = false;
            ImageView avatarIV = (ImageView) findViewById(R.id.recordBtn);


            Drawable drawable = ContextCompat.getDrawable(TripApplication.getAppContext(), R.drawable.circle_audio_record_normal_btn);

            avatarIV.setImageDrawable(drawable);

            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(mFileName);
                mediaPlayer.prepare(); // must call prepare first
                mediaPlayer.start(); // then start
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void goToBubbleMenu(View view) {
        addNewBubbleMenu("Menu");
    }
}
