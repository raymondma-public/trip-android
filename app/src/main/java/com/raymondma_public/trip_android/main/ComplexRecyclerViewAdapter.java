package com.raymondma_public.trip_android.main;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.blog.BlogActivity;
import com.raymondma_public.trip_android.community.CommunityActivity;
import com.raymondma_public.trip_android.model.Blog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by HEI on 15/10/2015.
 */
public class ComplexRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int Top = 0, Blog = 1;
    // The items to display in your RecyclerView
    private List<Object> items;
    private List<Blog> topBlogs;
    private ArrayList<String> bannerImages;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ComplexRecyclerViewAdapter(List<Object> items, List<Blog> topBlogs,ArrayList<String> bannerImages
     ) {
        this.items = items;
        this.topBlogs=topBlogs;
        this.bannerImages=bannerImages;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {

        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        //More to come
        if (items.get(position) instanceof String) {
            return Top;
        } else if (items.get(position) instanceof Blog) {
            return Blog;
        }
        return -1;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //More to come

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case Top:
                View v1 = inflater.inflate(R.layout.layout_viewholder_main_top, viewGroup, false);
                viewHolder = new ViewHolder1(v1,bannerImages);
                break;
            case Blog:
                View v2 = inflater.inflate(R.layout.layout_viewholder_main_recycle, viewGroup, false);
                viewHolder = new ViewHolder2(v2);
                break;
            default:
                View v = inflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
                viewHolder = new RecyclerViewSimpleTextViewHolder(v);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        //More to come
        switch (viewHolder.getItemViewType()) {
            case Top:
                ViewHolder1 vh1 = (ViewHolder1) viewHolder;
                configureViewHolder1(vh1, position);
                break;
            case Blog:
                ViewHolder2 vh2 = (ViewHolder2) viewHolder;
                configureViewHolder2(vh2,position);
                break;
            default:
                RecyclerViewSimpleTextViewHolder vh = (RecyclerViewSimpleTextViewHolder) viewHolder;
                configureDefaultViewHolder(vh, position);
                break;
        }

    }

    private void configureDefaultViewHolder(RecyclerViewSimpleTextViewHolder vh, int position) {
        vh.getLabel().setText((CharSequence) items.get(position));
    }

    private void configureViewHolder1(ViewHolder1 topVH, int position) {
//        topVH.getImageView().setImageResource(R.drawable.community_btn);

        if(topBlogs.size()>0) {
            if (topBlogs.get(0) != null) {
                Picasso.with(TripApplication.getAppContext()).load(topBlogs.get(0).getRandomImgURL()).skipMemoryCache().into(topVH.getBlog1IV());
                topVH.getBlog1IV().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent= new Intent(TripApplication.getAppContext(),BlogActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id", topBlogs.get(0).getId());
                        TripApplication.getAppContext().startActivity(intent);
                    }
                });
            }
            if (topBlogs.get(1) != null) {
                Picasso.with(TripApplication.getAppContext()).load(topBlogs.get(1).getRandomImgURL()).skipMemoryCache().into(topVH.getBlog2IV());

                topVH.getBlog2IV().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TripApplication.getAppContext(), BlogActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id", topBlogs.get(1).getId());
                        TripApplication.getAppContext().startActivity(intent);
                    }
                });
            }
            if (topBlogs.get(2) != null) {
                Picasso.with(TripApplication.getAppContext()).load(topBlogs.get(2).getRandomImgURL()).skipMemoryCache().into(topVH.getBlog3IV());
                topVH.getBlog3IV().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TripApplication.getAppContext(), BlogActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("id", topBlogs.get(2).getId());
                        TripApplication.getAppContext().startActivity(intent);
                    }
                });
            }
        }


    }

    private void configureViewHolder2(ViewHolder2 blogVH, int position) {
        final Blog blog = (Blog) items.get(position);
        if (blog != null) {

            blogVH.getTitleTV().setText( blog.getTitle());
            blogVH.getDescriptionTV().setText(blog.getDescription());
            Picasso.with(TripApplication.getAppContext()).load(blog.getRandomImgURL()).skipMemoryCache().into(  blogVH.getImageView());
            blogVH.getBlogItemLayout().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(TripApplication.getAppContext(),BlogActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id",blog.getId());
                    TripApplication.getAppContext().startActivity(intent);

                }
            });
        }

    }


}