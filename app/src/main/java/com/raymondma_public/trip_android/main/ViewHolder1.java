package com.raymondma_public.trip_android.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HEI on 15/10/2015.
 */
public class ViewHolder1 extends RecyclerView.ViewHolder implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    //    Slider
    private SliderLayout mDemoSlider;

    private TextView label1, label2;


    private ImageView blog1IV, blog2IV, blog3IV;

    public ViewHolder1(View v,ArrayList<String> bannerImages) {
        super(v);
        label1 = (TextView) v.findViewById(R.id.text1);
        label2 = (TextView) v.findViewById(R.id.text2);

        blog1IV = (ImageView) v.findViewById(R.id.firstBlog);
        blog2IV = (ImageView) v.findViewById(R.id.secondBlog);
        blog3IV = (ImageView) v.findViewById(R.id.thirdBlog);

        mDemoSlider = (SliderLayout) v.findViewById(R.id.slider);

        HashMap<String, String> url_maps = new HashMap<String, String>();
//        url_maps.put("Hannibal", "http://ad.qyer.com/www/images/2776e31c528623580b6284a5e7026265.jpg");
////        url_maps.put("Big Bang Theory", "http://ad.qyer.com/www/images/2776e31c528623580b6284a5e7026265.jpg");
//        url_maps.put("House of Cards", "http://ad.qyer.com/www/images/03673b84c256832d5980e59e6456a48d.jpg");
//        url_maps.put("Game of Thrones", "http://ad.qyer.com/www/images/bbfbaa44c695f9d6af4e631b9b4cba85.jpg");
        for(int i=0;i<bannerImages.size();i++){
            url_maps.put(""+i, bannerImages.get(i));
        }
        for (String name : url_maps.keySet()) {

            TextSliderView textSliderView = new TextSliderView(TripApplication.getAppContext());
            // initialize a SliderLayout
            textSliderView
//                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
//        mDemoSlider.setCustomAnimation(new Animation);
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
    }

    public TextView getLabel1() {
        return label1;
    }

    public void setLabel1(TextView label1) {
        this.label1 = label1;
    }

    public TextView getLabel2() {
        return label2;
    }

    public void setLabel2(TextView label2) {
        this.label2 = label2;
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }


    public ImageView getBlog1IV() {
        return blog1IV;
    }

    public ImageView getBlog2IV() {
        return blog2IV;
    }

    public ImageView getBlog3IV() {
        return blog3IV;
    }
}