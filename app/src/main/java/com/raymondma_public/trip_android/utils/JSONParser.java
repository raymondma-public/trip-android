package com.raymondma_public.trip_android.utils;

import com.google.api.client.json.Json;
import com.raymondma_public.trip_android.model.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by RaymondMa on 8/11/2015.
 */
public class JSONParser {
    private static JSONParser instance = new JSONParser();

    private JSONParser() {

    }

    public static JSONParser getInstance() {
        return instance;
    }

    public int getIntWithNameFromJSON(JSONObject jsonObject, String name) {
        int result = 0;
        try {
            result = jsonObject.getInt(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getStringWithNameFromJSON(JSONObject jsonObject, String name) {
        String result = "";
        try {
            result = jsonObject.getString(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Location getLocationWithNAmeFromJSON(JSONObject jsonObject, String name) {
        Location location = null;

        try {
            JSONObject locationJSON=jsonObject.getJSONObject("location");
            String locationName = locationJSON.getString("locationName");
            double latitude = locationJSON.getDouble("latitutde");
            double longitude = locationJSON.getDouble("longitude");


            location = new Location(locationName, latitude, longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return location;
    }

    public ArrayList<String> getArrayListStringFromJsonArray(JSONObject jsonObject, String arrayName) {
        ArrayList<String> results = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.getJSONArray(arrayName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            String tempString = null;
            try {
                tempString = jsonArray.getString(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            results.add(tempString);
        }
        return results;
    }
}
