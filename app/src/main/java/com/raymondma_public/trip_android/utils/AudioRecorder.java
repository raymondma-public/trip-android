package com.raymondma_public.trip_android.utils;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.raymondma_public.trip_android.TripApplication;

import java.io.IOException;
import java.io.RandomAccessFile;

public class AudioRecorder {
    private final Context myApp;

    public enum State {INITIALIZING, READY, RECORDING, ERROR, STOPPED}

    ;

    private byte[] audioBuffer = null;

    private int source = MediaRecorder.AudioSource.MIC;

    private int sampleRate = 0;

    private int encoder = 0;

    private int nChannels = 0;

    private int bufferRead = 0;

    private int bufferSize = 0;

    private RandomAccessFile tempAudioFile = null;

    public AudioRecord audioRecorder = null;

    private State state;

    private short bSamples = 16;

    private int framePeriod;

    // The interval in which the recorded samples are output to the file
// Used only in uncompressed mode
    private static final int TIMER_INTERVAL = 120;

    volatile Thread t = null;

    public int TimeStamp = 0, count = 0, preTimeStamp = 0;


    public AudioRecorder(Context c) {
        this.sampleRate = 11025;

        this.encoder = AudioFormat.ENCODING_PCM_16BIT;

//        this.nChannels = AudioFormat.CHANNEL_CONFIGURATION_MONO;
        this.nChannels =AudioFormat.CHANNEL_IN_MONO;

        this.preTimeStamp = (int) System.currentTimeMillis();

        myApp = TripApplication.getAppContext();

//        mQueue = TripApplication.getQueue();

        try {
/*          
      String fileName = "/sdcard/XYZ/11025.wav";

        tempAudioFile = new RandomAccessFile(fileName,"rw");
*/
            framePeriod = sampleRate * TIMER_INTERVAL / 1000;

            bufferSize = framePeriod * 2 * bSamples * nChannels / 8;

            if (bufferSize < AudioRecord.getMinBufferSize(sampleRate, nChannels, encoder)) {
                bufferSize = AudioRecord.getMinBufferSize(sampleRate, nChannels, encoder);

                // Set frame period and timer interval accordingly
                framePeriod = bufferSize / (2 * bSamples * nChannels / 8);

                Log.w(AudioRecorder.class.getName(), "Increasing buffer size to " + Integer.toString(bufferSize));
            }

            audioRecorder = new AudioRecord(source, sampleRate, nChannels, encoder, bufferSize);

            audioBuffer = new byte[2048];

            audioRecorder.setRecordPositionUpdateListener(updateListener);

            audioRecorder.setPositionNotificationPeriod(framePeriod);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private AudioRecord.OnRecordPositionUpdateListener updateListener = new AudioRecord.OnRecordPositionUpdateListener() {


        @Override
        public void onPeriodicNotification(AudioRecord recorder) {
//          Log.d(Constant.APP_LOG,"Into Periodic Notification...");

//            try {
//recorder.
//
//            } catch (IOException e)
//
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }


            if (recorder.getRecordingState()!=audioRecorder.RECORDSTATE_STOPPED)
            {
                audioBuffer = new byte[8000];
                bufferRead = recorder.read(audioBuffer, 0, 8000);

                if (bufferRead > 0)
                {
                    // do something
                }
            }

        }


        @Override
        public void onMarkerReached(AudioRecord recorder) {
            // TODO Auto-generated method stub

        }

    };




    public void start()
    {

        if(state==State.INITIALIZING)
        {

            audioRecorder.startRecording();

            state=State.RECORDING;


            t=new Thread()
            {
                public void run()
                {
                    //Here You can read your Audio Buffers
                    audioRecorder.read(audioBuffer,0,2048);
                }
            };

            t.setPriority(Thread.MAX_PRIORITY);

            t.start();


        }
        else
        {
            Log.e(AudioRecorder.class.getName(),"start() called on illegal state");

            state=State.ERROR;
        }

    }

    public void stop()
    {
        if(state==State.RECORDING)
        {

            audioRecorder.stop();

            Thread t1=t;

            t=null;

            t1.interrupt();

            count=0;

            state=State.STOPPED;

        }
        else
        {
            Log.e(AudioRecorder.class.getName(),"stop() called on illegal state");

            state=State.ERROR;
        }
    }

    public void release()
    {
        if(state==State.RECORDING)
        {
            stop();
        }

        if(audioRecorder!=null)
        {
            audioRecorder.release();

        }


    }

    public void reset()
    {
        try
        {
            if(state!=State.ERROR)
            {
                release();
            }
        }
        catch(Exception e)
        {
            Log.e(AudioRecorder.class.getName(),e.getMessage());

            state=State.ERROR;
        }
    }


    public State getState()
    {
        return state;
    }


}
