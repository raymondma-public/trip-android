package com.raymondma_public.trip_android.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.raymondma_public.trip_android.TripApplication;

import java.util.Calendar;

/**
 * Created by RaymondMa on 9/11/2015.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener
{

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

//        int hour=c.get(Calendar.HOUR);
//        int minute=c.get(Calendar.MINUTE);
//        boolean is24=true;
        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(TripApplication.getAppContext(), this, year, month, day);
//        return new TimePickerDialog(getActivity(),this,hour,minute);
//        return new TimePickerDialog(TripApplication.getAppContext(), new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                Toast.makeText(TripApplication.getAppContext(), hourOfDay+" : "+minute, Toast.LENGTH_SHORT).show();
//            }
//        },hour,minute, is24);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
    }
}