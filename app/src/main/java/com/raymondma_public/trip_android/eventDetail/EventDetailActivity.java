package com.raymondma_public.trip_android.eventDetail;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.raymondma_public.trip_android.Config;
import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.map.MapsActivity;
import com.raymondma_public.trip_android.model.Event;
import com.raymondma_public.trip_android.parsers.EventParser;
import com.raymondma_public.trip_android.volley.ErrorToastListener;
import com.raymondma_public.trip_android.volley.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EventDetailActivity extends BaseActivity {


    String TAG = "EventDetailActivity";
    EventParser eventParser = EventParser.getInstance();

    Event event;
    TextView titleTextView;
    ImageView mainImageView;
    TextView descriptionTV;

    Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {

                JSONArray products = response.getJSONArray("products");
                String allProduct = "";


                if (products.length() == 0) {
                    Toast.makeText(EventDetailActivity.this, "No Car", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < products.length(); i++) {
                        allProduct += ((JSONObject) products.get(0)).getString("description") + " ";

                    }
                    Toast.makeText(EventDetailActivity.this, allProduct, Toast.LENGTH_SHORT).show();
                }


            } catch (JSONException e) {
                Log.e("News", "Prase json error: " + e.getMessage());

            }
        }
    };


    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
            Toast.makeText(EventDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
            Log.e("News", "Load data error");
        }
    };


    Response.Listener<JSONObject> eventResponseListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            event = eventParser.getOneEventFromEventJSONObj(response);


            Picasso.with(TripApplication.getAppContext()).load(event.getRandomImgURL()).fit().into(mainImageView);

            if (event != null) {
                titleTextView.setText(event.getTitle());
                descriptionTV.setText(event.getDescription());
            }

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);


        View myActionBar = findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);


        Intent intent = getIntent();
        String id = (String) intent.getStringExtra("id");

        titleTextView = (TextView) findViewById(R.id.titleTV);
        mainImageView = (ImageView) findViewById(R.id.eventMainImage);
        descriptionTV = (TextView) findViewById(R.id.descriptionTV);


        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.GET_EVENT_URL + "?id=" + id, eventResponseListener, new ErrorToastListener());
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendRequest(View view) {
        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 37.775818 + "&longitude=" + -122.418028 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
//        JsonObjectRequest jsonNewRequest = new JsonObjectRequest(Request.Method.GET, Config.UBER_API + "?latitude=" + 0 + "&longitude=" + 0 + "&server_token=" + "Gni8eUn5cBOTzvgKg42YMZOsWbKVRp3YlpxrRymd", responseListener, errorListener);
        VolleySingleton.getInstance().getRequestQueue().add(jsonNewRequest);
    }


    public void goToUber(View view) {
        try {
            PackageManager pm = getBaseContext().getPackageManager();
            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
            String uri =
                    "uber://?action=setPickup&pickup=my_location&client_id=YOUR_CLIENT_ID";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            // No Uber app! Open mobile website.
            String url = "https://m.uber.com/sign-up?client_id=YOUR_CLIENT_ID";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    }


    public void goToMap(View view) {

        if (event != null && event.getLocation() != null && (event.getLocation().getLongitude() != 0 || event.getLocation().getLatitude() != 0)) {
            Intent intent = new Intent(this, MapsActivity.class);
            intent.putExtra("lat", event.getLocation().getLatitude());
            intent.putExtra("long", event.getLocation().getLongitude());
            startActivity(intent);
        }

    }
}
