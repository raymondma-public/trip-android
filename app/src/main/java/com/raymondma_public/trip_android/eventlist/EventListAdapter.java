package com.raymondma_public.trip_android.eventlist;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.TripApplication;
import com.raymondma_public.trip_android.eventDetail.EventDetailActivity;
import com.raymondma_public.trip_android.model.Event;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HEI on 16/10/2015.
 */
public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {
    Activity activity;
    private ArrayList<Event> mDataset;

    // Provide a suitable constructor (depends on the kind of dataset)
    public EventListAdapter(ArrayList<Event> myDataset, Activity activity) {
        mDataset = myDataset;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public EventListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);
        v.setMinimumHeight(170);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

//        Picasso.with(TripApplication.getAppContext()).load("http://i.imgur.com/DvpvklR.png").into(holder.imageViewLeft);
//        Picasso.with(TripApplication.getAppContext()).load("http://i.imgur.com/DvpvklR.png").into(holder.imageViewRight);
//
        holder.eventTitle.setText(mDataset.get(position).getTitle());
        holder.eventDescription.setText(mDataset.get(position).getDescription());
        String imgURL = mDataset.get(position).getRandomImgURL();
        Log.d("eventImg", imgURL);
        Picasso.with(TripApplication.getAppContext()).load(imgURL).fit().skipMemoryCache().into(holder.eventImg);

        holder.itemCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripApplication.getAppContext(), EventDetailActivity.class);
                intent.putExtra("id",mDataset.get(position).getID());
                activity.startActivity(intent);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CardView itemCard;
        TextView eventTitle;
        TextView eventDescription;
        ImageView eventImg;

        public ViewHolder(View v) {
            super(v);
            itemCard = (CardView) v.findViewById(R.id.eventItemCard);
            eventTitle = (TextView) v.findViewById(R.id.eventTitle);
            eventDescription = (TextView) v.findViewById(R.id.eventDescription);
            eventImg = (ImageView) v.findViewById(R.id.eventImg);
        }
    }
}