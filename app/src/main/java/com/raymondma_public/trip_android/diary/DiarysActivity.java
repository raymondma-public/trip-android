package com.raymondma_public.trip_android.diary;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.raymondma_public.trip_android.R;
import com.raymondma_public.trip_android.base.BaseActivity;
import com.raymondma_public.trip_android.database.TaskDbHelper;
import com.raymondma_public.trip_android.database.TaskEntry;
import com.raymondma_public.trip_android.eventlist.EventListAdapter;
import com.raymondma_public.trip_android.model.Event;
import com.raymondma_public.trip_android.model.Thing;
import com.raymondma_public.trip_android.parsers.EventParser;
import com.raymondma_public.trip_android.utils.TimeUtil;

import java.sql.Time;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/**
 * Created by RaymondMa on 15/11/2015.
 */
public class DiarysActivity extends BaseActivity {


    private TaskDbHelper dbHelper;
    private ArrayList<Thing> data = new ArrayList<>();


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diarys);


        View myActionBar = findViewById(R.id.my_action_bar);
        ImageView logoutBtn = (ImageView) myActionBar.findViewById(R.id.logoutBtn);
        ImageView loginBtn = (ImageView) myActionBar.findViewById(R.id.loginBtn);
        setLoginLogoutBtn(loginBtn, logoutBtn);


        mRecyclerView = (RecyclerView) findViewById(R.id.diaryRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        dbHelper = new TaskDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {TaskEntry.COLUMN_NAME_TASK_ID,
                TaskEntry.COLUMN_NAME_TASK_TITLE,
                TaskEntry.COLUMN_NAME_TASK_START_LAT,
                TaskEntry.COLUMN_NAME_TASK_START_LONG,
                TaskEntry.COLUMN_NAME_TASK_END_LAT,
                TaskEntry.COLUMN_NAME_TASK_END_LONG,
                TaskEntry.COLUMN_NAME_TASK_TYPE,
                TaskEntry.COLUMN_NAME_TASK_FILE_PATH,
                TaskEntry.COLUMN_NAME_TASK_DATE_TIME};

        final ArrayList<Long> tempData = new ArrayList();
        Cursor c = db.query(TaskEntry.TABLE_NAME, projection, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(0);
                String title = c.getString(1);
                double startLat = c.getDouble(2);
                double startLong = c.getDouble(3);
                double endLat = c.getDouble(4);
                double endLong = c.getDouble(5);
                String type = c.getString(6);
                String filePath = c.getString(7);
                long timeStamp = c.getLong(8);
                if (!checkContains(tempData, timeStamp)) {
                    tempData.add(timeStamp);
                    data.add(new Thing(id, title, startLat, startLong, endLat, endLong, type, filePath, timeStamp));
                }

            } while (c.moveToNext());
        }

        setData();


        for (Thing thing : data) {
            Log.d("title", thing.getTitle());
            Log.d("getFilePath", thing.getFilePath());
            Log.d("Id", "" + thing.getId());
            Log.d("date", "" + thing.getDay() + "/" + thing.getMonth() + "/" + thing.getYear());
        }


    }

    private boolean checkContains(ArrayList<Long> tempData, long dateCurrentTimeZone) {
        for (long value : tempData) {
            boolean dayEqual = TimeUtil.getDayFromTimestamp(value) == TimeUtil.getDayFromTimestamp(dateCurrentTimeZone);
            boolean monthEqual = TimeUtil.getMonthFromTimestamp(value) == TimeUtil.getMonthFromTimestamp(dateCurrentTimeZone);
            boolean yearEqual = TimeUtil.getYearFromTimestamp(value) == TimeUtil.getYearFromTimestamp(dateCurrentTimeZone);
            if (dayEqual && monthEqual && yearEqual) {
                return true;
            }

        }
        return false;
    }

    private void setData() {


        mAdapter = new DiaryAdapter(data, DiarysActivity.this);
        SlideInBottomAnimationAdapter slideInBottomAnimationAdapter = new SlideInBottomAnimationAdapter(mAdapter);
        AlphaInAnimationAdapter alphaInAnimationAdapter = new AlphaInAnimationAdapter(slideInBottomAnimationAdapter);
        alphaInAnimationAdapter.setFirstOnly(false);
        mRecyclerView.setAdapter(alphaInAnimationAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
