package com.raymondma_public.trip_android.diary;

/**
 * Created by RaymondMa on 16/11/2015.
 */
public class Diary {
    private int year;
    private int month;

    private int day;

    public Diary(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
