package com.raymondma_public.trip_android.database;

import android.provider.BaseColumns;

/**
 * Created by RaymondMa on 15/11/2015.
 */
public abstract class TaskEntry implements BaseColumns {
    public static final String TABLE_NAME = "Thing";
    public static final String COLUMN_NAME_TASK_ID = "tid";

    public static final String COLUMN_NAME_TASK_TITLE = "title";
    public static final String COLUMN_NAME_TASK_TYPE = "type";
    public static final String COLUMN_NAME_TASK_FILE_PATH = "path";

    public static final String COLUMN_NAME_TASK_START_LAT = "startLat";
    public static final String COLUMN_NAME_TASK_START_LONG = "startLong";
    public static final String COLUMN_NAME_TASK_END_LAT = "endLat";
    public static final String COLUMN_NAME_TASK_END_LONG = "endLong";

    public static final String COLUMN_NAME_TASK_DATE_TIME = "created_at";


    private TaskEntry() {
    }

    ; //prevents class being instantiated

}