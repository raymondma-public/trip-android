package com.raymondma_public.trip_android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by RaymondMa on 15/11/2015.
 */
public class TaskDbHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Task.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TaskEntry.TABLE_NAME + " (" +
                    TaskEntry._ID + " INTEGER PRIMARY KEY," +
                    TaskEntry.COLUMN_NAME_TASK_ID + TEXT_TYPE + COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_TITLE + TEXT_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_START_LAT + REAL_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_START_LONG + REAL_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_TYPE + TEXT_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_FILE_PATH + TEXT_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_END_LAT + REAL_TYPE +COMMA_SEP +
                    TaskEntry.COLUMN_NAME_TASK_END_LONG + REAL_TYPE +COMMA_SEP +

                    "created_at DATETIME DEFAULT CURRENT_TIMESTAMP" +
                    ");";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TaskEntry.TABLE_NAME;

    public TaskDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("SQL_CREATE_ENTRIES", SQL_CREATE_ENTRIES);
        db = getWritableDatabase();
    }

    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_ENTRIES);
        Log.d("onCreateDB", "onCreateDB");
        Log.d("onCreateDB",SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {        // This database is only a cache for online data, so its  //upgrade policy is  to simply to discard the data and  //start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}